import React from 'react';

import NavigationItem from './NavigationItem/NavigationItem';
import classes from './NavigationItems.module.css';

const navigationItems = (props) => (
    <ul className={classes.NavigationsItems}>
       <NavigationItem link='/' >Burger Builder</NavigationItem>
       <NavigationItem link='/orders' >Order</NavigationItem>
    </ul>
);

export default navigationItems;