import React from 'react';

import classes from './Button.module.css';

const button = (props) => {
    // console.log("In button commponent " + props.disabled)
    return(
        <button 
            
            disabled ={props.disabled}
            onClick={props.clicked } className={[classes.Button, classes[props.btnType] ].join(' ')}>
            {props.children}
        </button>
)};

export default button;