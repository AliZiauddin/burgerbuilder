import React, { Component } from 'react';

import Aux from '../Auxiliary';
import classes from './Layout.module.css'
import Toolbar from '../../components/Navigation/Toolbar/Toolbar'
import SideDrawer from '../../components/Navigation/Toolbar/SideDrawer/SideDrawer';

class Layout extends Component  {
    state = {
         showSideDrawer : false
    }

    SideDrawerClosedHandler = () => {
        this.setState({showSideDrawer:false}); 
    }

    ToggleSideDrawer = () => {
        this.setState((prevState)=>{
            return {showSideDrawer: !prevState.showSideDrawer};
        });
    }

    render(){
        return(
            <Aux>
                <Toolbar ToggleSideDrawer={this.ToggleSideDrawer}/>
                <SideDrawer 
                    open={this.state.showSideDrawer}  
                    closed={this.SideDrawerClosedHandler} />
                <main className={classes.Content}>
                    {this.props.children}
                </main>
            </Aux>
        )
    }       
}


export default Layout;